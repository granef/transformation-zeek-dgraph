#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse x509.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a x509 log.

    :return: Defined schema as a string.
    """
    schema = """\
        x509.fingerprint: string .
        x509.certificate_version: int .
        x509.certificate_serial: string .
        x509.certificate_subject: string .
        x509.certificate_issuer: string .
        x509.certificate_not_valid_before: dateTime .
        x509.certificate_not_valid_after: dateTime .
        x509.certificate_key_alg: string .
        x509.certificate_sig_alg: string .
        x509.certificate_key_type: string .
        x509.certificate_key_length: int .
        x509.certificate_exponent: int .
        x509.basic_constraints_ca: bool .
        x509.host_cert: bool .
        x509.client_cert: bool .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("X509", ["x509.fingerprint",
                      "x509.certificate_version",
                      "x509.certificate_serial",
                      "x509.certificate_subject",
                      "x509.certificate_issuer",
                      "x509.certificate_not_valid_before",
                      "x509.certificate_not_valid_after",
                      "x509.certificate_key_alg",
                      "x509.certificate_sig_alg",
                      "x509.certificate_key_type",
                      "x509.certificate_key_length",
                      "x509.certificate_exponent",
                      "x509.basic_constraints_ca",
                      "x509.host_cert",
                      "x509.client_cert"])]


def get_mutations(x509_json, source_name):
    """
    Parsing of the x509 log file.

    :param x509_json: Loaded JSON with a x509 log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given x509 node
    """
    id_x509 = "cert-" + x509_json.get("fingerprint", "")
    nquads_mutation = textwrap.dedent("""\
        _:{id_x509} <source> "{data_source}" .
        _:{id_x509} <x509.fingerprint> "{fingerprint}" .
        _:{id_x509} <x509.certificate_version> "{certificate_version}" .
        _:{id_x509} <x509.certificate_serial> "{certificate_serial}" .
        _:{id_x509} <x509.certificate_subject> "{certificate_subject}" .
        _:{id_x509} <x509.certificate_issuer> "{certificate_issuer}" .
        _:{id_x509} <x509.certificate_not_valid_before> "{certificate_not_valid_before}" .
        _:{id_x509} <x509.certificate_not_valid_after> "{certificate_not_valid_after}" .
        _:{id_x509} <x509.certificate_key_alg> "{certificate_key_alg}" .
        _:{id_x509} <x509.certificate_sig_alg> "{certificate_sig_alg}" .
        _:{id_x509} <x509.certificate_key_type> "{certificate_key_type}" .
        _:{id_x509} <x509.certificate_key_length> "{certificate_key_length}" .
        _:{id_x509} <x509.certificate_exponent> "{certificate_exponent}" .
        _:{id_x509} <x509.basic_constraints_ca> "{basic_constraints_ca}" .
        _:{id_x509} <x509.host_cert> "{host_cert}" .
        _:{id_x509} <x509.client_cert> "{client_cert}" .
        _:{id_x509} <dgraph.type> "X509" .
    """.format(
        data_source=source_name,
        id_x509=id_x509,
        fingerprint=x509_json.get("fingerprint", ""),
        certificate_version=x509_json.get("certificate.version", 0),
        certificate_serial=x509_json.get("certificate.serial", ""),
        certificate_subject=x509_json.get("certificate.subject", "").replace('"', '').replace('\\', '-'),
        certificate_issuer=x509_json.get("certificate.issuer", "").replace('\\', '-'),
        certificate_not_valid_before=x509_json.get("certificate.not_valid_before", "1970-01-01T00:00:00.000000Z"),
        certificate_not_valid_after=x509_json.get("certificate.not_valid_after", "1970-01-01T00:00:00.000000Z"),
        certificate_key_alg=x509_json.get("certificate.key_alg", ""),
        certificate_sig_alg=x509_json.get("certificate.sig_alg", ""),
        certificate_key_type=x509_json.get("certificate.key_type", ""),
        certificate_key_length=x509_json.get("certificate.key_length", 0),
        certificate_exponent=x509_json.get("certificate.exponent", 0),
        basic_constraints_ca=str(x509_json.get("basic_constraints.ca", False)).lower(),
        host_cert=str(x509_json.get("host_cert", False)).lower(),
        client_cert=str(x509_json.get("client_cert", False)).lower(),
    ))

    return textwrap.dedent(nquads_mutation)
