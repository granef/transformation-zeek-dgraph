#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2022  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse dns.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings
import ipaddress  # IP address processing
import validators  # IP and hostname validators
import hashlib  # Hash operations


def get_schema():
    """
    Definition of Dgraph schema for a domain names of given host and a dns log.

    Value host.domain is specified as a array of strings (behaves as set) and indexed as trigram to allow filtering
    by regular expressions.

    Value dns.query is indexed as trigram to allow filtering by regular expressions.

    :return: Defined schema as a string.
    """
    schema = """\
        dns.query: string @index(trigram, exact) .
        dns.qclass: int .
        dns.qclass_name: string .
        dns.qtype: int .
        dns.qtype_name: string .
        dns.rcode: int .
        dns.rcode_name: string .
        dns.aa: bool .
        dns.tc: bool .
        dns.rd: bool .
        dns.ra: bool .
        dns.z: int .
        dns.answers: [string] .
        dns.rejected: bool .
        dns.hostname: [uid] @reverse .
        host.hostname: [uid] @reverse .
        hostname.name: string @index(trigram, exact) .
        hostname.type: [string] @index(exact) .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Dns", ["dns.query",
                     "dns.qclass",
                     "dns.qclass_name",
                     "dns.qtype",
                     "dns.qtype_name",
                     "dns.rcode",
                     "dns.rcode_name"
                     "dns.aa",
                     "dns.tc",
                     "dns.rd",
                     "dns.ra",
                     "dns.z",
                     "dns.answers",
                     "dns.rejected",
                     "dns.hostname",
                     "<~connection.produced>"]),
            ("Host", ["host.hostname"]),
            ("Hostname", ["hostname.name",
                          "hostname.type",
                          "<~dns.hostname>",
                          "<~host.hostname>"])]


def get_mutations(dns_json, source_name):
    """
    Parsing of the dns log file and definition of all host-domain name pairs in the nquads format.

    Data model: (IP)--connection.produced--dns-->[domain_names]

    :param dns_json: Loaded JSON with a dns log line from the Zeek log
    :param source_name: Name of the data source
    :return: Nquads definition for host and all his domain names
    """
    id_dns = "dns" + dns_json.get("uid", "").replace(':', '_0')
    qtype_name = dns_json.get("qtype_name", "")
    query = dns_json.get("query", "")
    answers = dns_json.get("answers", [])
    nquads_mutation = textwrap.dedent("""\
        _:{id_dns} <source> "{data_source}" .
        _:{id_dns} <dgraph.type> "Dns" .
        _:{id_dns} <dns.query> "{query}" .
        _:{id_dns} <dns.qclass> "{qclass}" .
        _:{id_dns} <dns.qclass_name> "{qclass_name}" .
        _:{id_dns} <dns.qtype> "{qtype}" .
        _:{id_dns} <dns.qtype_name> "{qtype_name}" .
        _:{id_dns} <dns.rcode> "{rcode}" .
        _:{id_dns} <dns.rcode_name> "{rcode_name}" .
        _:{id_dns} <dns.aa> "{aa}" .
        _:{id_dns} <dns.tc> "{tc}" .
        _:{id_dns} <dns.rd> "{rd}" .
        _:{id_dns} <dns.ra> "{ra}" .
        _:{id_dns} <dns.z> "{z}" .
        _:{id_dns} <dns.rejected> "{rejected}" .
        _:{connection_uid} <connection.produced> _:{id_dns} .
    """.format(
        data_source=source_name,
        id_dns=id_dns,
        query=query,
        qclass=dns_json.get("qclass", 0),
        qclass_name=dns_json.get("qclass_name", ""),
        qtype=dns_json.get("qtype", 0),
        qtype_name=qtype_name,
        rcode=dns_json.get("rcode", 0),
        rcode_name=dns_json.get("rcode_name", "NOERROR"),
        aa=str(dns_json.get("AA", False)).lower(),
        tc=str(dns_json.get("TC", False)).lower(),
        rd=str(dns_json.get("RD", False)).lower(),
        ra=str(dns_json.get("RA", False)).lower(),
        z=dns_json.get("Z", 0),
        rejected=str(dns_json.get("rejected", False)).lower(),
        connection_uid=dns_json.get("uid", "").replace(':', '_0')
    ))

    for answer in answers:
        # Do not process special characters in the answer string 
        nquads_mutation += textwrap.dedent("""\
            _:{id_dns} <dns.answers> "{answer}" .
        """.format(
            id_dns=id_dns,
            answer=repr(answer)[1:-1]
        ))

    if qtype_name == "A" or qtype_name == "AAAA":  # IPv4, IPv6
        for answer in answers:
            try:
                ipaddress.ip_address(answer)
                nquads_mutation += textwrap.dedent("""\
                    _:{id_dns} <dns.hostname> _:{hostname_id} .
                    _:{orig_h} <host.hostname> _:{hostname_id} .
                    _:{hostname_id} <hostname.name> "{hostname}" .
                    _:{hostname_id} <hostname.type> "dns" .
                    _:{hostname_id} <source> "{data_source}" .
                    _:{hostname_id} <dgraph.type> "Hostname" .
                """.format(
                    id_dns=id_dns,
                    orig_h=answer.replace(':', '_0'),
                    hostname_id="hname-" + hashlib.sha1(query.replace(':', '_0').encode('utf-8')).hexdigest(),
                    hostname=query,
                    data_source=source_name
                ))
            except ValueError:
                continue

    return textwrap.dedent(nquads_mutation)
