#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse http.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings
import hashlib  # Hash operations


def get_schema():
    """
    Definition of Dgraph schema for a smpt log.

    :return: Defined schema as a string.
    """
    schema = """\
        smtp.ts: dateTime @index(hour) .
        smtp.orig_p: int @index(int) .
        smtp.resp_p: int @index(int) .
        smtp.trans_depth: int .
        smtp.helo: [string] .
        smtp.mailfrom: string @index(trigram, exact) .
        smtp.rcptto: [string] .
        smtp.from: string @index(trigram, exact) .
        smtp.to: [string] .
        smtp.subject: string @index(trigram) .
        smtp.last_reply: string @index(trigram) .
        smtp.tls: bool .
        smtp.resp_fuid: [uid] @count @reverse .
        smtp.is_webmail: bool .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Smtp", ["smtp.ts",
                      "smtp.orig_p",
                      "smtp.resp_p",
                      "smtp.trans_depth",
                      "smtp.helo",
                      "smtp.mailfrom",
                      "smtp.rcptto",
                      "smtp.from",
                      "smtp.to",
                      "smtp.subject",
                      "smtp.last_reply",
                      "smtp.tls",
                      "smtp.is_webmail",
                      "<~connection.produced>"]),
            ("Files", ["<~smtp.resp_fuid>"])
            ]


def get_mutations(smtp_json, source_name):
    """
    Parsing of the smpt log file and definition of file, user_agent and hostname as distinct nodes.

    :param smtp_json: Loaded JSON with a smtp log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given smtp node
    """
    id_smtp = "smtp" + smtp_json.get("uid", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_smtp} <source> "{data_source}" .
        _:{id_smtp} <smtp.ts> "{ts}" .
        _:{id_smtp} <smtp.orig_p> "{orig_p}" .
        _:{id_smtp} <smtp.resp_p> "{resp_p}" .
        _:{id_smtp} <smtp.trans_depth> "{trans_depth}" .
        _:{id_smtp} <smtp.helo> "{helo}" .
        _:{id_smtp} <smtp.mailfrom> "{mailfrom}" .
        _:{id_smtp} <smtp.rcptto> "{rcptto}" .
        _:{id_smtp} <smtp.from> "{m_from}" .
        _:{id_smtp} <smtp.to> "{to}" .
        _:{id_smtp} <smtp.subject> "{subject}" .
        _:{id_smtp} <smtp.last_reply> "{last_reply}" .
        _:{id_smtp} <smtp.tls> "{tls}" .
        _:{id_smtp} <smtp.is_webmail> "{is_webmail}" .
        _:{id_smtp} <dgraph.type> "Smtp" .
        _:{connection_uid} <connection.produced> _:{id_smtp} .
    """.format(
        data_source=source_name,
        id_smtp=id_smtp,
        ts=smtp_json.get("ts", "1970-01-01T00:00:00.000000Z"),
        orig_p=smtp_json.get("orig_p", 0),
        resp_p=smtp_json.get("resp_p", 0),
        trans_depth=smtp_json.get("trans_depth", 0),
        helo=smtp_json.get("helo", ""),
        mailfrom=smtp_json.get("mailfrom", ""),
        rcptto=smtp_json.get("rcptto", ""),
        m_from=smtp_json.get("from", ""),
        to=smtp_json.get("to", ""),
        subject=smtp_json.get("subject", ""),
        last_reply=smtp_json.get("last_reply", ""),
        tls=str(smtp_json.get("tls", False)).lower(),
        is_webmail=str(smtp_json.get("is_webmail", False)).lower(),
        connection_uid=smtp_json.get("uid", "").replace(':', '_0')
    ))

    return textwrap.dedent(nquads_mutation)
