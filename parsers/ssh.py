#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse ssh.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a ssh log.

    :return: Defined schema as a string.
    """
    schema = """\
        ssh.version: int .
        ssh.auth_success: bool .
        ssh.auth_attempts: int .
        ssh.client: string .
        ssh.server: string .
        ssh.cipher_alg: string .
        ssh.mac_alg: string .
        ssh.compression_alg: string .
        ssh.kex_alg: string .
        ssh.host_key_alg: string .
        ssh.host_key: string .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Ssh", ["ssh.version",
                     "ssh.auth_success",
                     "ssh.auth_attempts",
                     "ssh.client",
                     "ssh.server",
                     "ssh.cipher_alg",
                     "ssh.mac_alg",
                     "ssh.compression_alg",
                     "ssh.kex_alg",
                     "ssh.host_key_alg",
                     "ssh.host_key",
                     "<~connection.produced>"])]


def get_mutations(ssh_json, source_name):
    """
    Parsing of the ssh log file with the difinition of file as a distinct node.

    :param ssh_json: Loaded JSON with a ssh log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given ssh node
    """
    id_ssh = "ssh" + ssh_json.get("uid", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_ssh} <source> "{data_source}" .
        _:{id_ssh} <dgraph.type> "Ssh" .
        _:{id_ssh} <ssh.version> "{version}" .
        _:{id_ssh} <ssh.auth_access> "{auth_success}" .
        _:{id_ssh} <ssh.auth_attempts> "{auth_attempts}" .
        _:{id_ssh} <ssh.client> "{client}" .
        _:{id_ssh} <ssh.server> "{server}" .
        _:{id_ssh} <ssh.cipher_alg> "{cipher_alg}" .
        _:{id_ssh} <ssh.mac_alg> "{mac_alg}" .
        _:{id_ssh} <ssh.compression_alg> "{compression_alg}" .
        _:{id_ssh} <ssh.kex_alg> "{kex_alg}" .
        _:{id_ssh} <ssh.host_key_alg> "{host_key_alg}" .
        _:{id_ssh} <ssh.host_key> "{host_key}" .
        _:{connection_uid} <connection.produced> _:{id_ssh} .
    """.format(
        data_source=source_name,
        id_ssh=id_ssh,
        version=ssh_json.get("version", 0),
        auth_success=str(ssh_json.get("auth_success", False)).lower(),
        auth_attempts=ssh_json.get("auth_attempts", 0),
        client=ssh_json.get("client", "").replace('\n', '').replace('\t', '').replace('\r', '').replace('"', '\'').replace('\\', '\\\\'),
        server=ssh_json.get("server", ""),
        cipher_alg=ssh_json.get("cipher_alg", ""),
        mac_alg=ssh_json.get("mac_alg", ""),
        compression_alg=ssh_json.get("compression_alg", ""),
        kex_alg=ssh_json.get("kex_alg", ""),
        host_key_alg=ssh_json.get("host_key_alg", ""),
        host_key=ssh_json.get("host_key", ""),
        connection_uid=ssh_json.get("uid", "").replace(':', '_0')
    ))

    return textwrap.dedent(nquads_mutation)
