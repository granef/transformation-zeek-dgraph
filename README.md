<img  src="https://is.muni.cz/www/milan.cermak/granef/granef-logo.svg"  height="60px">

[**Graph-Based Network Forensics**](https://gitlab.ics.muni.cz/granef/granef)**: Zeek transformation**

---
  
The Transformation Module takes the log files that were generated from the [Extraction Module](https://gitlab.ics.muni.cz/granef/extraction-zeek) as an input and transforms their content to triples (in [RDF N-Quad](https://www.w3.org/TR/n-quads/) format) which can be later used as an input to the [Dgraph](https://dgraph.io/) graph database.

The transformation is applied to a pre-defined set of log files. The currently handled files are: `conn`, `http`, `dns`, `ssl`, `notice`, `ntp`, `sip`, `rdp`, `x509`, `files`, `ftp`, `ssh`, `kerberos`, `smb_files`, `smb_mapping`, `smtp`, and `software`. If a new log file is supposed to be handled as well, a corresponding [parser](/parsers/) has to be defined. 

#### Database schema

The log files' content is transformed to a format corresponding to the database schema available at:

[https://is.muni.cz/www/milan.cermak/granef/dgraph-schema_no-ioc.html](https://is.muni.cz/www/milan.cermak/granef/dgraph-schema_no-ioc.html)
 

### Requirements

- Docker 
- Python3
- Python3 packages in [requirements.txt](requirements.txt)

The installation can be performed using the following command:

```bash
$ git clone https://gitlab.ics.muni.cz/granef/transformation-zeek-dgraph.git
```

Use the following command to build the Docker container:

```bash
$ docker build --tag=granef/transformation-zeek-dgraph --pull .
```

### Usage

The Docker container can be either run separately with command line arguments or as part of the Granef toolkit with arguments set in the [granef.yml](https://gitlab.ics.muni.cz/granef/granef/-/blob/master/granef.yml) configuration file. 

The following arguments can be set:

| Short argument | Long argument | Description | Default | Required |
|-|-|-|-|-|
|`-i`|`--input`|Input data directory path|`/data/`|T|
|`-o`|`--output`|Output data directory path|`granef-transformation/zeek/`|F|
|`-m`|`--mounted`|Mounted data directory path|`/data/`|F|
|`-rf`|`--rdf_file_name`|Output RDF file name|`zeek-dgraph.rdf`|F|
|`-sf`|`--schema_file_name`|Output schema file name|`zeek-dgraph.schema`|F|
|`-c`|`--case_id`|Case ID to store as a source to each node|`granef`|F|
|`-f`|`--force`|Forces to overwrite files in output directory||F|
|`-l`|`--log`|Log level (possible values: `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`)|`INFO`|F|

Use the following command to start the transformation:

```bash
$ docker run --rm -v <LOCAL_DIR>:/data/ granef/transformation-zeek-dgraph -i <INPUT_PATH> -o <OUTPUT_PATH> -rf <RDF_FILE_NAME> -sf <SCHEMA_FILE_NAME> -c <CASE_ID>
```

The Zeek transformation module then performs the following steps:

1. Finds log files in JSON format (specified as input) in the local directory.
2. Transforms such files (if there are any) using [zeek-dgraph.py](zeek-dgraph.py).
3. Saves the resulting `.schema` and `.rdf` files to the local output directory.
