#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse smb_mapping.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a smb_mapping log.

    :return: Defined schema as a string.
    """
    schema = """\
        smb_mapping.path: string .
        smb_mapping.service: string .
        smb_mapping.share_type: string .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Smb_mapping", ["smb_mapping.path",
                             "smb_mapping.service",
                             "smb_mapping.share_type",
                             "<~connection.produced>"])]


def get_mutations(smb_mapping_json, source_name):
    """
    Parsing of the smb_mapping log file with the difinition of file as a distinct node.

    :param smb_mapping_json: Loaded JSON with a smb_mapping log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given smb_mapping node
    """
    id_smb_mapping = "smbm" + smb_mapping_json.get("id", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_smb_mapping} <source> "{data_source}" .
        _:{id_smb_mapping} <dgraph.type> "Smb_mapping" .
        _:{id_smb_mapping} <smb_mapping.path> "{path}" .
        _:{id_smb_mapping} <smb_mapping.service> "{service}" .
        _:{id_smb_mapping} <smb_mapping.share_type> "{share_type}" .
        _:{connection_uid} <connection.produced> _:{id_smb_mapping} .
    """.format(
        data_source=source_name,
        id_smb_mapping=id_smb_mapping,
        path=smb_mapping_json.get("path", "").replace('\\', '\\\\'),
        service=smb_mapping_json.get("service", ""),
        share_type=smb_mapping_json.get("share_type", ""),
        connection_uid=smb_mapping_json.get("uid", "").replace(':', '_0')
    ))

    return textwrap.dedent(nquads_mutation)
