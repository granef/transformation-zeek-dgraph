#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse rdp.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a rdp log.

    :return: Defined schema as a string.
    """
    schema = """\
        rdp.cookie: string .
        rdp.result: string .
        rdp.security_protocol: string .
        rdp.cert_count: int .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Rdp", ["rdp.cookie",
                     "rdp.result",
                     "rdp.security_protocol",
                     "rdp.cert_count",
                     "<~connection.produced>"])]


def get_mutations(rdp_json, source_name):
    """
    Parsing of the rdp log file and definition of file, user_agent and hostname as distinct nodes.

    :param rdp_json: Loaded JSON with a rdp log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given rdp node
    """
    id_rdp = "rdp" + rdp_json.get("uid", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_rdp} <source> "{data_source}" .
        _:{id_rdp} <dgraph.type> "Rdp" .
        _:{id_rdp} <rdp.cookie> "{cookie}" .
        _:{id_rdp} <rdp.result> "{result}" .
        _:{id_rdp} <rdp.security_protocol> "{security_protocol}" .
        _:{id_rdp} <rdp.cert_count> "{cert_count}" .
        _:{connection_uid} <connection.produced> _:{id_rdp} .
    """.format(
        data_source=source_name,
        id_rdp=id_rdp,
        connection_uid=rdp_json.get("uid", "").replace(':', '_0'),
        cookie=rdp_json.get("cookie", "").replace('\\', '\\\\').replace('"', '').replace(':', '_0'),
        result=rdp_json.get("result", ""),
        security_protocol=rdp_json.get("security_protocol", ""),
        cert_count=rdp_json.get("cert_count", 0)
    ))

    return textwrap.dedent(nquads_mutation)
