#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse ssl.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings
import hashlib  # Hash operations


def get_schema():
    """
    Definition of Dgraph schema for a ssl log.

    :return: Defined schema as a string.
    """
    schema = """\
        ssl.version: string .
        ssl.cipher: string .
        ssl.resumed: bool .
        ssl.established: bool .
        ssl.sni_matches_cert: bool .
        ssl.validation_status: string .
        ssl.cert_chain_fps: [uid] @reverse .
        host.x509: [uid] @reverse .
        x509.fingerprint: string .
        host.hostname: [uid] @reverse .
        hostname.name: string @index(trigram, exact) .
        hostname.type: [string] @index(exact) .
        ssl.hostname: [uid] @reverse .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Ssl", ["ssl.hostname",
                     "ssl.cert_chain_fps",
                     "ssl.version",
                     "ssl.cipher",
                     "ssl.resumed",
                     "ssl.established",
                     "ssl.sni_matches_cert",
                     "ssl.validation_status",
                     "<~connection.produced>"]),
            ("Host", ["host.hostname",
                      "host.x509"]),
            ("Hostname", ["hostname.name",
                          "hostname.type",
                          "<~host.hostname>",
                          "<~ssl.hostname>"]),
            ("X509", ["x509.fingerprint",
                      "<~ssl.cert_chain_fps>",
                      "<~host.x509>"])]


def get_mutations(ssl_json, source_name):
    """
    Parsing of the ssl log file and definition of x509 and hostname as distinct nodes.

    :param ssl_json: Loaded JSON with a ssl log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given ssl node
    """
    id_ssl = "ssl" + ssl_json.get("uid", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_ssl} <source> "{data_source}" .
        _:{id_ssl} <dgraph.type> "Ssl" .
        _:{id_ssl} <ssl.version> "{version}" .
        _:{id_ssl} <ssl.cipher> "{cipher}" .
        _:{id_ssl} <ssl.resumed> "{resumed}" .
        _:{id_ssl} <ssl.established> "{established}" .
        _:{id_ssl} <ssl.sni_matches_cert> "{sni_matches_cert}" .
        _:{id_ssl} <ssl.validation_status> "{validation_status}" .
        _:{connection_uid} <connection.produced> _:{id_ssl} .
    """.format(
        data_source=source_name,
        id_ssl=id_ssl,
        version=ssl_json.get("version", ""),
        cipher=ssl_json.get("cipher", ""),
        resumed=str(ssl_json.get("resumed", False)).lower(),
        established=str(ssl_json.get("established", False)).lower(),
        sni_matches_cert=str(ssl_json.get("sni_matches_cert", False)).lower(),
        validation_status=ssl_json.get("validation_status", ""),
        connection_uid=ssl_json.get("uid", "").replace(':', '_0')
    ))

    if "cert_chain_fps" in ssl_json or "client_cert_chain_fps" in ssl_json:
        for cert_chain in ["cert_chain_fps", "client_cert_chain_fps"]:
            for fingerprint in ssl_json.get(cert_chain, []):
                nquads_mutation += textwrap.dedent("""\
                    _:{id_ssl} <ssl.cert_chain_fps> _:{id_cert} .
                    _:{host} <host.x509> _:{id_cert} .
                    _:{id_cert} <x509.fingerprint> "{fingerprint}" .
                    _:{id_cert} <source> "{data_source}" .
                    _:{id_cert} <dgraph.type> "X509" .
                """.format(
                    id_ssl=id_ssl,
                    id_cert="cert-"+fingerprint,
                    host=(ssl_json.get("id.resp_h", "") if cert_chain == "cert_chain_fps" else ssl_json.get("id.orig_h", "")).replace(':', '_0'),
                    fingerprint=fingerprint,
                    data_source=source_name
                ))

    # Add hostname to the host
    if "server_name" in ssl_json:
        nquads_mutation += textwrap.dedent("""\
            _:{host} <host.hostname> _:{hostname_id} .
            _:{hostname_id} <hostname.name> "{hostname}" .
            _:{hostname_id} <hostname.type> "ssl" .
            _:{hostname_id} <source> "{data_source}" .
            _:{hostname_id} <dgraph.type> "Hostname" .
            _:{id_ssl} <ssl.hostname> _:{hostname_id} .
        """.format(
            host=ssl_json.get("id.resp_h", "").replace(':', '_0'),
            hostname_id="hname-" + hashlib.sha1(ssl_json.get("server_name", "").replace(':', '_0').encode('utf-8')).hexdigest(),
            hostname=ssl_json.get("server_name", ""),
            id_ssl=id_ssl,
            data_source=source_name
        ))

    return textwrap.dedent(nquads_mutation)
