#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Application to create RDF and schema files used for bulk data import into the Dgraph database.


Log file parser can be found in ./parsers directory named according to parsed log. To enable new
log type parsing, add a new file with the name of the log providing get_schema() and
get_mutations(json, source_name) functions.

Usage:
    $ ./zeek-dgraph.py -s "source" -l "./data/input/"
"""

import sys  # Common system functions
import os  # Common Operating system functions
import errno  # Common return codes
import argparse  # Arguments parser
import json  # Common JSON parser
import multiprocessing  # Multiprocessing functions
import textwrap  # dedent() function to trim multiline strings
import logging, coloredlogs


def process_log_file(log_file, log_name, source_name, data_queue):
    """
    Subprocess function to parse given log file, get mutations, and store results in the given queue.

    The function is able to loads parsing modules dynamically according to the name of the Zeek
    file. If the processing module is not available, the function prints a Warning message.

    :param log_file: Path of the Zeek log file that will be processes
    :param log_name: Name of the log file without path and extension
    :param source_name: value of <source> attribute
    :param data_queue: Multiprocess queue where resulting RDF data will be stored
    """
    # Load mutations module
    try:
        parser = __import__("parsers." + log_name, fromlist=[''])
    except ModuleNotFoundError:
        logger.warning("Mutations for " + log_name + ".log are not defined")
        return

    with open(log_file, 'r') as file:
        for log_line in file:
            data_queue.put(parser.get_mutations(json.loads(log_line), source_name))


def write_rdf_file(rdf_file, data_queue):
    """
    Subprocess function to write data from the given queue to the output file.
    
    The function is prepared to run as a separate queue listening process that will end together
    with the main program process. The function needs to be started as a daemon process:
        multiprocessing.Process(target=write_rdf_file, args=(<file>,<queue>), daemon=True)
    
    :param rdf_file: Path of the output RDF file
    :param data_queue: Multiprocess queue with data that should be saved to the RDF file
    """
    with open(rdf_file, "w", encoding='utf-8') as rdf_file_handler:
        while True:
            data = data_queue.get()
            rdf_file_handler.write(data)
            rdf_file_handler.flush()
            data_queue.task_done()


def unify_schema_types(schema_types):
    dict = {}
    for type_name, type_details in schema_types:
        if type_name not in dict:
            dict[type_name] = type_details
        else:
            old_details = dict[type_name]
            new_details = list(set(old_details) | set(type_details))  # Union Dgraph type details
            dict[type_name] = new_details
    return dict


def format_schema_types(schema_types):
    types_string = ""
    for key in schema_types:
        types_string += "type " + str(key) + " {\n"
        for detail in schema_types[key]:
            types_string += textwrap.dedent("""\
                {detail}
            """.format(
                detail=detail))
        types_string += "}\n\n"
    return types_string


if __name__ == "__main__":
    # Define application arguments (automatically creates -h argument)
    parser = argparse.ArgumentParser()
    parser.add_argument("-or", "--output_rdf", help="Output RDF file", required=False, default="zeek-dgraph.rdf")
    parser.add_argument("-os", "--output_schema", help="Output schema file",
                        type=argparse.FileType('w', encoding='utf-8'), required=False, default="zeek-dgraph.schema")
    parser.add_argument("-s", "--source", metavar="CASE_NAME", help="Case name to store as a source to each node", required=False, default="granef", type=str)
    parser.add_argument("-ld", "--logs_directory", help="Path to directory with Zeek log files", required=True)
    parser.add_argument('-l', '--log', choices=['debug', 'info', 'warning', 'error', 'critical'], help='Log level',
                        default='INFO')
    args = parser.parse_args()

    log = args.log
    logger = logging.getLogger("Transformation-Zeek-Dgraph")
    coloredlogs.install(level=getattr(logging, log.upper()), fmt='%(asctime)s %(name)s [%(levelname)s]: %(message)s')

    # List and store info about all files in given Zeek logs directory
    log_files = [f for f in os.listdir(args.logs_directory) if f.endswith('.log')]
    if "conn.log" not in log_files:
        logger.error("Conn.log file not found in directory: " + args.logs_directory)
        sys.exit(errno.ENOENT)

    # Get Dgraph schema file for each Zeek log file
    dgraph_schema = ""
    dgraph_types = []
    for log_file in log_files:
        try:
            parser = __import__("parsers." + log_file.split(".")[0], fromlist=[''])
            dgraph_schema += parser.get_schema()

            # Get Dgraph schema types from each parser
            parsed_types = parser.get_types()
            for parsed_type in parsed_types:
                dgraph_types.append(parsed_type)

        except ModuleNotFoundError:
            logger.warning("Schema for " + log_file + " is not defined")

    dgraph_schema = '\n'.join(sorted(set(dgraph_schema.splitlines())))  # Remove duplicates

    # Unify Dgraph schema types
    types_dict = unify_schema_types(dgraph_types)
    formatted_types = format_schema_types(types_dict)
    dgraph_schema += "\n\n" + formatted_types

    with args.output_schema as output_schema:
        output_schema.write(dgraph_schema)

    # Get Dgraph RDF file for each Zeek log file
    data_queue = multiprocessing.JoinableQueue()  # Multiprocessing queue allowing sync via join()

    write_process = multiprocessing.Process(target=write_rdf_file, args=(args.output_rdf, data_queue),
                                            daemon=True)  # Demon process will be destroyed together with the main program
    write_process.start()

    processing_processes = []
    for log_file in log_files:
        log_name = log_file.split(".")[0]
        temporary_file_name = "output_" + log_name + ".rdf"
        processing_process = multiprocessing.Process(target=process_log_file, args=(
            args.logs_directory + "/" + log_file, log_name, args.source, data_queue))
        processing_processes.append(processing_process)
        processing_process.start()

    for processing_process in processing_processes:
        processing_process.join()

    data_queue.join()  # Wait till all data in the queue are processed
