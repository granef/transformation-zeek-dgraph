#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse ftp.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a ftp log.

    :return: Defined schema as a string.
    """
    schema = """\
        ftp.user: string .
        ftp.password: string .
        ftp.command: string .
        ftp.arg: string .
        ftp.mime_type: string .
        ftp.reply_code: int .
        ftp.reply_msg: string .
        ftp.data_channel_passive: bool .
        ftp.data_channel_orig_h: string .
        ftp.data_channel_resp_h: string .
        ftp.data_channel_resp_p: int .
        ftp.fuid: [uid] @reverse .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Ftp", ["ftp.user",
                     "ftp.password",
                     "ftp.command",
                     "ftp.arg",
                     "ftp.mime_type",
                     "ftp.reply_code",
                     "ftp.reply_msg",
                     "ftp.data_channel_passive",
                     "ftp.data_channel_orig_h",
                     "ftp.data_channel_resp_h",
                     "ftp.data_channel_resp_p",
                     "ftp.fuid"
                     "<~connection.produced>"]),
            ("File", ["<~ftp.fuid>"])]


def get_mutations(ftp_json, source_name):
    """
    Parsing of the ftp log file with the definition of file as a distinct node.

    :param ftp_json: Loaded JSON with a ftp log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given ftp node
    """
    id_ftp = "ftp-" + ftp_json.get("uid", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_ftp} <source> "{data_source}" .
        _:{id_ftp} <type> "ftp" .
        _:{id_ftp} <ftp.user> "{user}" .
        _:{id_ftp} <ftp.password> "{password}" .
        _:{id_ftp} <ftp.command> "{command}" .
        _:{id_ftp} <ftp.arg> "{arg}" .
        _:{id_ftp} <ftp.mime_type> "{mime_type}" .
        _:{id_ftp} <ftp.reply_code> "{reply_code}" .
        _:{id_ftp} <ftp.reply_msg> "{reply_msg}" .
        _:{id_ftp} <ftp.data_channel_passive> "{data_channel_passive}" .
        _:{id_ftp} <ftp.data_channel_orig_h> "{data_channel_orig_h}" .
        _:{id_ftp} <ftp.data_channel_resp_h> "{data_channel_resp_h}" .
        _:{id_ftp} <ftp.data_channel_resp_p> "{data_channel_resp_p}" .
        _:{connection_uid} <connection.produced> _:{id_ftp} .
    """.format(
        data_source=source_name,
        id_ftp=id_ftp,
        user=ftp_json.get("user", ""),
        password=ftp_json.get("password", ""),
        command=ftp_json.get("command", ""),
        arg=ftp_json.get("arg", ""),
        mime_type=ftp_json.get("mime_type", ""),
        reply_code=ftp_json.get("reply_code", 0),
        reply_msg=ftp_json.get("reply_msg", ""),
        data_channel_passive=str(ftp_json.get("data_channel.passive", False)).lower(),
        data_channel_orig_h=ftp_json.get("data_channel.orig_h", ""),
        data_channel_resp_h=ftp_json.get("data_channel.resp_h", ""),
        data_channel_resp_p=ftp_json.get("data_channel.resp_p", 0),
        connection_uid=ftp_json.get("uid", "").replace(':', '_0')
    ))

    return textwrap.dedent(nquads_mutation)
