#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Script:
1. Finds log files (specified as input) from local directory in JSON format.
2. Those files (if there are any) are then transformed using zeek-dgraph.py.
3. The result is saved to a local directory called 'transformation'.

Usage:=
* Obtain data via local directory:
    $ communication_handler.py -i <input_dir_path> -o <output_dir_path> -rf <rdf_file_name> -sf <schema_file_name>
      -c <case_id>
"""


import sys
import re
import os
import subprocess
import argparse
import shutil
import logging, coloredlogs


def fix_permissions(path):
    os.chmod(path, 0o0777)
    for subdir, _, files in os.walk(path):
        for file in files:
            file_path = os.path.join(subdir, file)
            os.chmod(file_path, 0o0666)


def output_dir_already_contains_output_file(output_dir_path, output_file_extensions):
    """
        Checks if an output file ends with a given extension in the output directory.

        :param output_dir_path: directory that is searched
        :param output_file_extensions: specifies what file(s) to look for based on their ending(s)
        :return: True if all files that end with given extensions exist, False otherwise
    """
    files_counter = {}
    for file_extension in output_file_extensions:
        files_counter[file_extension] = 0

    for file in os.listdir(output_dir_path):
        for file_extension in output_file_extensions:
            if file.endswith(file_extension):
                files_counter[file_extension] += 1

    # return True if each expected file is present
    return len([files_counter[i] for i in files_counter if files_counter[i] >= 1]) == len(output_file_extensions)


def get_zeek_directory(input_directory: str) -> str:
    """
        Get directory with Zeek logs in given input_directory path. Searches for conn.log file which should be present in the directory.
        
        :param input_directory (str): Path to directory where to search for zeek logs.
        :return str: Path to directory with Zeek logs, None otherwise.
    """
    # Check given input directory and zeek subdirectory
    for directory in [input_directory, input_directory + "/zeek/"]:
        for filename in os.listdir(directory):
            if filename == "conn.log":
                return directory
    return None


if __name__ == '__main__':
    # Define application arguments (automatically creates -h argument)
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='Input data directory path', required=True, default='/data/', type=str)
    parser.add_argument('-o', '--output', help='Output data directory path', default='granef-transformation/zeek/', type=str)
    parser.add_argument('-m', '--mounted', help='Mounted data directory path', default='/data/', type=str)
    parser.add_argument('-rf', '--rdf_file_name', help='Output RDF file name', default='zeek-dgraph.rdf', type=str)
    parser.add_argument('-sf', '--schema_file_name', help='Output schema file name', default='zeek-dgraph.schema',
                        type=str)
    parser.add_argument("-s", "--source", metavar="CASE_NAME", help="Case name to store as a source to each node",
                        required=False, default="granef", type=str)
    parser.add_argument('-f', '--force', help='Forces to overwrite any files that are already present in the '
                                              'output data directory', action='store_true')
    parser.add_argument('-l', '--log', choices=['debug', 'info', 'warning', 'error', 'critical'], help='Log level',
                        default='INFO')
    args = parser.parse_args()

    # Set logging
    logger = logging.getLogger("Transformation")
    coloredlogs.install(level=getattr(logging, args.log.upper()), fmt='%(asctime)s %(name)s [%(levelname)s]: %(message)s')

    # TODO - if new log files are supposed to be transformed, add their names to log_file_names:
    log_file_names = ('conn', 'http', 'dns', 'ssl', 'notice', 'sip', 'x509', 'files', 'ftp',
                      'ssh', 'kerberos', 'smb_files', 'smb_mapping', 'smtp')
    input = args.input
    output = args.mounted + '/' + args.output
    output_tmp = '/tmp/' + args.output

    if args.input.replace('/', '') != args.mounted.replace('/', ''):
        input = args.mounted + '/' + args.input
    logger.debug('Arguments set to: input = ' + input + ', output = ' + output + ', source = ' + args.source)

    # Get directories with Zeek logs and other event files
    zeek_directory = get_zeek_directory(input)
    if not zeek_directory:
        logging.error("Directory with Zeek logs not found")
        exit()

    # Copy Zeek directory to /tmp/
    zeek_directory_tmp = '/tmp/zeek/'
    shutil.copytree(zeek_directory, zeek_directory_tmp)
    logger.debug('Input data successfully copied to the /tmp/ directory')

    if not os.path.isdir(output):
        os.makedirs(output)
        os.makedirs(output_tmp)

    if output_dir_already_contains_output_file(output, ['.rdf', '.schema']) and not args.force:
        # if the output directory does already exist, it is checked whether it does
        # not already contain the output files (then the user is warned)
        logger.warning('The output directory (' + output + ') seems to already contain the corresponding output '
                    'file(s). If you wish to run this Module anyway, run it with the -f/--force option to '
                    'overwrite the current content of the output directory.')
        exit()

    output_rdf_file = output_tmp + '/' + args.rdf_file_name
    output_schema_file = output_tmp + '/' + args.schema_file_name

    if not output_rdf_file.endswith('.rdf'):
        output_rdf_file += '.rdf'
    if not output_schema_file.endswith('.schema'):
        output_schema_file += '.schema'
    
    try:
        # apply the transformation on log files in input_dir:
        subprocess.check_call(' python3 /usr/local/bin/granef/zeek-dgraph.py' + ' -l ' + args.log.lower() + ' -ld ' + zeek_directory_tmp +
                        ' -or ' + output_rdf_file + ' -os ' + output_schema_file + ' -s \'' + args.source + '\'',
                        shell=True)
        logger.debug('Output files successfully generated: ' + output_rdf_file + ', ' + output_schema_file)
        shutil.copytree(output_tmp, output, dirs_exist_ok=True)
        fix_permissions(output)

    except subprocess.CalledProcessError as e:
        logger.error('(CalledProcessError) When running zeek-dgraph.py (errno=' + str(e.errno) + ', strerror' +
                str(e.strerror) + ', filename' + str(e.filename) + ').')
    except Exception as e:
        logger.error('When running zeek-dgraph.py (' + str(e) + ').')
