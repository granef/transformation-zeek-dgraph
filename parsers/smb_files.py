#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse smb_files.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a smb_files log.

    :return: Defined schema as a string.
    """
    schema = """\
        smb_files.action: string .
        smb_files.path: string .
        smb_files.name: string .
        smb_files.size: int .
        smb_files.times_modified: dateTime .
        smb_files.times_accessed: dateTime .
        smb_files.times_created: dateTime .
        smb_files.times_changed: dateTime .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Smb_files", ["smb_files.action",
                           "smb_files.path",
                           "smb_files.name",
                           "smb_files.size",
                           "smb_files.times_modified",
                           "smb_files.times_accessed",
                           "smb_files.times_created",
                           "smb_files.times_changed",
                           "<~connection.produced>"])]


def get_mutations(smb_files_json, source_name):
    """
    Parsing of the smb_files log file with the difinition of file as a distinct node.

    :param smb_files_json: Loaded JSON with a smb_files log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given smb_files node
    """
    id_smb_files = "smbf" + smb_files_json.get("id", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_smb_files} <source> "{data_source}" .
        _:{id_smb_files} <dgraph.type> "Smb_files" .
        _:{id_smb_files} <smb_files.action> "{action}" .
        _:{id_smb_files} <smb_files.path> "{path}" .
        _:{id_smb_files} <smb_files.name> "{name}" .
        _:{id_smb_files} <smb_files.size> "{size}" .
        _:{connection_uid} <connection.produced> _:{id_smb_files} .
    """.format(
        data_source=source_name,
        id_smb_files=id_smb_files,
        action=smb_files_json.get("action", ""),
        path=smb_files_json.get("path", "").replace('\\', '\\\\'),
        name=smb_files_json.get("name", "").replace('\\', '\\\\'),
        size=smb_files_json.get("size", 0),
        connection_uid=smb_files_json.get("uid", "").replace(':', '_0')
    ))

    if smb_files_json.get("times.modified"):
        nquads_mutation += textwrap.dedent("""\
            _:{id_smb_files} <smb_files.times_modified> "{times_modified}" .
        """.format(
            id_smb_files=id_smb_files,
            times_modified=smb_files_json.get("times.modified", "1970-01-01T00:00:00.000000Z")
        ))

    if smb_files_json.get("times.accessed"):
        nquads_mutation += textwrap.dedent("""\
            _:{id_smb_files} <smb_files.times_accessed> "{times_accessed}" .
        """.format(
            id_smb_files=id_smb_files,
            times_accessed=smb_files_json.get("times.accessed", "1970-01-01T00:00:00.000000Z")
        ))

    if smb_files_json.get("times.created"):
        nquads_mutation += textwrap.dedent("""\
            _:{id_smb_files} <smb_files.times_created> "{times_created}" .
        """.format(
            id_smb_files=id_smb_files,
            times_created=smb_files_json.get("times.created", "1970-01-01T00:00:00.000000Z")
        ))

    if smb_files_json.get("times.changed"):
        nquads_mutation += textwrap.dedent("""\
            _:{id_smb_files} <smb_files.times_changed> "{times_changed}" .
        """.format(
            id_smb_files=id_smb_files,
            times_changed=smb_files_json.get("times.changed", "1970-01-01T00:00:00.000000Z")
        ))

    return textwrap.dedent(nquads_mutation)
