#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse conn.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a connection log.

    Values host.originated and host.responded are indexed as reverse to allow bidirectional analysis. Value host.ip is
    indexed as trigram to allow filtering by regular expressions.

    :return: Defined schema as a string.
    """
    schema = """\
        source: string @index(exact, term) .
        host.originated: [uid] @count @reverse .
        host.responded: [uid] @count @reverse .
        host.communicated: [uid] @count @reverse .
        host.ip: string @index(cidr, exact) .
        connection.uid: string .
        connection.ts: dateTime @index(hour) .
        connection.orig_p: int @index(int) .
        connection.resp_p: int @index(int) .
        connection.proto: string @index(trigram, term) .
        connection.service: string .
        connection.duration: float @index(float) .
        connection.orig_bytes: int @index(int) .
        connection.resp_bytes: int @index(int) .
        connection.conn_state: string @index(exact) .
        connection.orig_pkts: int .
        connection.orig_ip_bytes: int @index(int) .
        connection.resp_pkts: int .
        connection.resp_ip_bytes: int @index(int) .
        connection.produced: [uid] @reverse .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Connection", ["connection.source",
                            "connection.uid",
                            "connection.ts",
                            "connection.orig_p",
                            "connection.resp_p",
                            "connection.proto",
                            "connection.service",
                            "connection.duration",
                            "connection.orig_bytes",
                            "connection.resp_bytes",
                            "connection.conn_state",
                            "connection.orig_pkts",
                            "connection.orig_ip_bytes",
                            "connection.resp_pkts",
                            "connection.resp_ip_bytes",
                            "connection.produced",
                            "<~host.originated>",
                            "<~host.responded>"]),
            ("Host", ["host.originated",
                      "host.responded",
                      "host.communicated",
                      "<~host.communicated>",
                      "host.ip"])]


def get_mutations(conn_json, source_name):
    """
    Parsing of the connection log file and definition of all connection in the nquads format with hosts as
    distinct nodes.

    Data model: (IP)--Originated-->(Connection)<--Responded--(IP)

    :param conn_json: Loaded JSON with a connection log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given connection
    """
    # Skip connections with empty hosts (bro marks them as "::")
    if conn_json.get("id.orig_h") == "::" or conn_json.get("id.resp_h") == "::":
        return ""

    # N-Quads mutation (conn_json.get() used to return None if a key does not exists)
    nquads_mutation = textwrap.dedent("""\
        _:{uid} <source> "{data_source}" .
        _:{uid} <connection.uid> "{uid}" .
        _:{uid} <connection.ts> "{ts}" .
        _:{uid} <connection.orig_p> "{orig_p}" .
        _:{uid} <connection.resp_p> "{resp_p}" .
        _:{uid} <connection.proto> "{proto}" .
        _:{uid} <connection.service> "{service}" .
        _:{uid} <connection.duration> "{duration}" .
        _:{uid} <connection.orig_bytes> "{orig_bytes}" .
        _:{uid} <connection.resp_bytes> "{resp_bytes}" .
        _:{uid} <connection.conn_state> "{conn_state}" .
        _:{uid} <connection.orig_pkts> "{orig_pkts}" .
        _:{uid} <connection.orig_ip_bytes> "{orig_ip_bytes}" .
        _:{uid} <connection.resp_pkts> "{resp_pkts}" .
        _:{uid} <connection.resp_ip_bytes> "{resp_ip_bytes}" .
        _:{uid} <dgraph.type> "Connection" .
        _:{orig_h} <source> "{data_source}" .
        _:{orig_h} <host.ip> "{orig_h}" .
        _:{orig_h} <host.originated> _:{uid} .
        _:{orig_h} <dgraph.type> "Host" .
        _:{resp_h} <source> "{data_source}" .
        _:{resp_h} <host.ip> "{resp_h}" .
        _:{resp_h} <host.responded> _:{uid} .
        _:{resp_h} <dgraph.type> "Host" .
        _:{orig_h} <host.communicated> _:{resp_h} .
    """.format(
        data_source=source_name,
        uid=conn_json.get("uid", "").replace(':', '_0'),
        # Conversion to '_0' because:
        # https://github.com/dgraph-io/dgraph/blob/master/chunker/rdf/parse.go - func sane = at least one char
        # needs to be alfanumeric
        # https://www.w3.org/TR/n-quads/ - rdf blank nodes syntax uid
        ts=conn_json.get("ts", "1970-01-01T00:00:00.000000Z"),
        orig_p=conn_json.get("id.orig_p", 0),
        resp_p=conn_json.get("id.resp_p", 0),
        proto=conn_json.get("proto", ""),
        service=conn_json.get("service", ""),
        duration=conn_json.get("duration", 0),
        orig_bytes=conn_json.get("orig_bytes", 0),
        resp_bytes=conn_json.get("resp_bytes", 0),
        conn_state=conn_json.get("conn_state", ""),
        orig_pkts=conn_json.get("orig_pkts", 0),
        orig_ip_bytes=conn_json.get("orig_ip_bytes", 0),
        resp_pkts=conn_json.get("resp_pkts", 0),
        resp_ip_bytes=conn_json.get("resp_ip_bytes", 0),
        orig_h=conn_json.get("id.orig_h", "").replace(':', '_0'),
        resp_h=conn_json.get("id.resp_h", "").replace(':', '_0')
    ))

    return textwrap.dedent(nquads_mutation)
