#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse http.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings
import hashlib  # Hash operations


def get_schema():
    """
    Definition of Dgraph schema for a http log.

    Values http.uri, user_agent.name and hostname.name is indexed as trigram to allow filtering by regular expressions.

    :return: Defined schema as a string.
    """
    schema = """\
        http.trans_depth: int .
        http.method: string @index(exact) .
        http.version: float .
        http.uri: string @index(trigram) .
        http.referrer: string .
        http.request_body_len: int .
        http.response_body_len: int .
        http.status_code: int @index(int) .
        http.status_msg: string @index(exact) .
        http.resp_mime_types: [string] .
        http.resp_fuid: [uid] @count @reverse .
        host.user_agent: [uid] @reverse .
        user_agent.name: string @index(trigram, exact) .
        user_agent.type: string @index(exact) .
        http.user_agent: [uid] @reverse .
        host.hostname: [uid] @reverse .
        hostname.name: string @index(trigram, exact) .
        hostname.type: [string] @index(exact) .
        http.hostname: [uid] @reverse .
        http.hostname_uri: string @index(trigram) .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Http", ["http.trans_depth",
                      "http.method",
                      "http.version",
                      "http.uri",
                      "http.referrer",
                      "http.request_body_len",
                      "http.response_body_len",
                      "http.status_code",
                      "http.status_msg",
                      "http.resp_mime_types",
                      "http.resp_fuid",
                      "http.user_agent",
                      "http.hostname",
                      "<~connection.produced>",
                      "http.hostname_uri"]),
            ("User_Agent", ["user_agent.name",
                            "user_agent.type",
                            "<~http.user_agent>",
                            "<~host.user_agent>"]),
            ("Hostname", ["hostname.name",
                          "hostname.type",
                          "<~http.hostname>",
                          "<~host.hostname>"]),
            ("Host", ["host.hostname",
                      "host.user_agent"]),
            ("Files", ["<~http.resp_fuid>"])]


def get_mutations(http_json, source_name):
    """
    Parsing of the http log file and definition of file, user_agent and hostname as distinct nodes.

    :param http_json: Loaded JSON with a http log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given http node
    """
    id_http = "http-" + http_json.get("uid", "").replace(':', '_0')
    uri_http = http_json.get("uri", "").replace('\n', '').replace('\t', '').replace('\r', '').replace('"',
                                                                                                      '\'').replace(
        '\\', '\\\\')
    hostname_uri = http_json.get("host", "") + uri_http
    nquads_mutation = textwrap.dedent("""\
        _:{id_http} <source> "{data_source}" .
        _:{id_http} <http.trans_depth> "{trans_depth}" .
        _:{id_http} <http.method> "{method}" .
        _:{id_http} <http.version> "{version}" .
        _:{id_http} <http.uri> "{uri}" .
        _:{id_http} <http.referrer> "{referrer}" .
        _:{id_http} <http.request_body_len> "{request_body_len}" .
        _:{id_http} <http.response_body_len> "{response_body_len}" .
        _:{id_http} <http.status_code> "{status_code}" .
        _:{id_http} <http.status_msg> "{status_msg}" .
        _:{id_http} <http.resp_mime_types> "{resp_mime_types}" .
        _:{id_http} <http.hostname_uri> "{hostname_uri}" .
        _:{id_http} <dgraph.type> "Http" .
        _:{connection_uid} <connection.produced> _:{id_http} .
    """.format(
        data_source=source_name,
        id_http=id_http,
        trans_depth=http_json.get("trans_depth", 0),
        method=http_json.get("method", ""),
        version=http_json.get("version", 0.0),
        uri=uri_http,
        referrer=http_json.get("referrer", "").replace('\\', '\\\\'),
        request_body_len=http_json.get("request_body_len", 0),
        response_body_len=http_json.get("response_body_len", 0),
        status_code=http_json.get("status_code", 0),
        status_msg=http_json.get("status_msg", ""),
        resp_mime_types=http_json.get("resp_mime_types", ""),
        hostname_uri=hostname_uri,
        connection_uid=http_json.get("uid", "").replace(':', '_0')
    ))

    # Add user-agent to the host
    if "user_agent" in http_json:
        nquads_mutation += textwrap.dedent("""\
            _:{host} <host.user_agent> _:{ua_id} .
            _:{ua_id} <source> "{data_source}" .
            _:{ua_id} <user_agent.name> "{user_agent}" .
            _:{ua_id} <user_agent.type> "http" .
            _:{id_http} <http.user_agent> _:{ua_id} .
            _:{ua_id} <dgraph.type> "User_Agent" .
        """.format(
            host=http_json.get("id.orig_h", "").replace(':', '_0'),
            ua_id="ua-" + hashlib.sha1(
                http_json.get("user_agent").replace('"', '').replace(':', '_0').encode('utf-8')).hexdigest(),
            data_source=source_name,
            user_agent=http_json.get("user_agent", "").replace('"', ''),
            id_http=id_http
        ))

    # Add hostname to the host
    if "host" in http_json:
        nquads_mutation += textwrap.dedent("""\
            _:{host} <host.hostname> _:{hostname_id} .
            _:{hostname_id} <hostname.name> "{hostname}" .
            _:{hostname_id} <hostname.type> "http" .
            _:{hostname_id} <source> "{data_source}" .
            _:{hostname_id} <dgraph.type> "Hostname" .
            _:{id_http} <http.hostname> _:{hostname_id} .
        """.format(
            host=http_json.get("id.resp_h", "").replace(':', '_0'),
            hostname_id="hname-" + hashlib.sha1(http_json.get("host").replace(':', '_0').encode('utf-8')).hexdigest(),
            data_source=source_name,
            hostname=http_json.get("host", ""),
            id_http=id_http
        ))

    return textwrap.dedent(nquads_mutation)
