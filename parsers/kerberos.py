#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse kerberos.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a kerberos log.

    :return: Defined schema as a string.
    """
    schema = """\
        kerberos.request_type: string .
        kerberos.client: string .
        kerberos.service: string .
        kerberos.success: bool .
        kerberos.error_msg: string .
        kerberos.till: dateTime .
        kerberos.cipher: string .
        kerberos.forwardable: bool .
        kerberos.renewable: bool .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Kerberos", ["kerberos.request_type",
                          "kerberos.client",
                          "kerberos.service",
                          "kerberos.success",
                          "kerberos.error_msg",
                          "kerberos.till",
                          "kerberos.cipher",
                          "kerberos.forwardable",
                          "kerberos.renewable",
                          "<~connection.produced>"])]


def get_mutations(kerberos_json, source_name):
    """
    Parsing of the kerberos log file with the difinition of file as a distinct node.

    :param kerberos_json: Loaded JSON with a kerberos log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given kerberos node
    """
    id_kerberos = "kerb" + kerberos_json.get("uid", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_kerberos} <source> "{data_source}" .
        _:{id_kerberos} <dgraph.type> "Kerberos" .
        _:{id_kerberos} <kerberos.request_type> "{request_type}" .
        _:{id_kerberos} <kerberos.client> "{client}" .
        _:{id_kerberos} <kerberos.service> "{service}" .
        _:{id_kerberos} <kerberos.success> "{success}" .
        _:{id_kerberos} <kerberos.error_msg> "{error_msg}" .
        _:{id_kerberos} <kerberos.till> "{till}" .
        _:{id_kerberos} <kerberos.cipher> "{cipher}" .
        _:{id_kerberos} <kerberos.forwardable> "{forwardable}" .
        _:{id_kerberos} <kerberos.renewable> "{renewable}" .
        _:{connection_uid} <connection.produced> _:{id_kerberos} .
    """.format(
        data_source=source_name,
        id_kerberos=id_kerberos,
        request_type=kerberos_json.get("request_type", ""),
        client=kerberos_json.get("client", ""),
        service=kerberos_json.get("service", ""),
        success=str(kerberos_json.get("success", False)).lower(),
        error_msg=kerberos_json.get("error_msg", ""),
        till=kerberos_json.get("till", "1970-01-01T00:00:00.000000Z"),
        cipher=kerberos_json.get("cipher", ""),
        forwardable=str(kerberos_json.get("forwardable", False)).lower(),
        renewable=str(kerberos_json.get("renewable", False)).lower(),
        connection_uid=kerberos_json.get("uid", "").replace(':', '_0')
    ))

    return textwrap.dedent(nquads_mutation)
