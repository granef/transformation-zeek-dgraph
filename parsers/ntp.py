#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse ntp.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a ntp log.

    :return: Defined schema as a string.
    """
    schema = """\
        ntp.version: int .
        ntp.mode: int .
        ntp.stratum: int .
        ntp.poll: float .
        ntp.precision: string .
        ntp.root_delay: string .
        ntp.root_disp: string .
        ntp.ref_id: string .
        ntp.ref_time: dateTime .
        ntp.org_time: dateTime .
        ntp.rec_time: dateTime .
        ntp.xmt_time: dateTime .
        ntp.num_exts: int .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Ntp", ["ntp.version",
                     "ntp.mode",
                     "ntp.stratum",
                     "ntp.poll",
                     "ntp.precision",
                     "ntp.root_delay",
                     "ntp.root_disp",
                     "ntp.ref_id",
                     "ntp.ref_time",
                     "ntp.org_time",
                     "ntp.rec_time",
                     "ntp.xmt_time",
                     "ntp.num_exts",
                     "<~connection.produced>"])]


def get_mutations(ntp_json, source_name):
    """
    Parsing of the ntp log file and definition of file, user_agent and hostname as distinct nodes.

    :param ntp_json: Loaded JSON with a ntp log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given ntp node
    """
    # Added stratum to preserve all ogs for the given uid
    id_ntp = "ntp" + ntp_json.get("uid", "").replace(':', '_0') + str(ntp_json.get("stratum", 0))
    nquads_mutation = textwrap.dedent("""\
        _:{id_ntp} <source> "{data_source}" .
        _:{id_ntp} <dgraph.type> "Ntp" .
        _:{id_ntp} <ntp.version> "{version}" .
        _:{id_ntp} <ntp.mode> "{mode}" .
        _:{id_ntp} <ntp.stratum> "{stratum}" .
        _:{id_ntp} <ntp.poll> "{poll}" .
        _:{id_ntp} <ntp.precision> "{precision}" .
        _:{id_ntp} <ntp.root_delay> "{root_delay}" .
        _:{id_ntp} <ntp.root_disp> "{root_disp}" .
        _:{id_ntp} <ntp.ref_id> "{ref_id}" .
        _:{id_ntp} <ntp.ref_time> "{ref_time}" .
        _:{id_ntp} <ntp.org_time> "{org_time}" .
        _:{id_ntp} <ntp.rec_time> "{rec_time}" .
        _:{id_ntp} <ntp.xmt_time> "{xmt_time}" .
        _:{id_ntp} <ntp.num_exts> "{num_exts}" .
        _:{connection_uid} <connection.produced> _:{id_ntp} .
    """.format(
        data_source=source_name,
        id_ntp=id_ntp,
        connection_uid=ntp_json.get("uid", "").replace(':', '_0'),
        version=ntp_json.get("version", 0),
        mode=ntp_json.get("mode", 0),
        stratum=ntp_json.get("stratum", 0),
        poll=ntp_json.get("poll", 0),
        precision=str(ntp_json.get("precision", 0)),
        root_delay=str(ntp_json.get("root_delay", 0)),
        root_disp=str(ntp_json.get("root_disp", 0)),
        ref_id=ntp_json.get("ref_id", ""),
        ref_time=ntp_json.get("ref_time", "1970-01-01T00:00:00.000000Z"),
        org_time=ntp_json.get("org_time", "1970-01-01T00:00:00.000000Z"),
        rec_time=ntp_json.get("rec_time", "1970-01-01T00:00:00.000000Z"),
        xmt_time=ntp_json.get("xmt_time", "1970-01-01T00:00:00.000000Z"),
        num_exts=ntp_json.get("num_exts", 0)
    ))

    return textwrap.dedent(nquads_mutation)
