#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse sip.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings
import hashlib  # Hash operations


def get_schema():
    """
    Definition of Dgraph schema for a sip log.

    :return: Defined schema as a string.
    """
    schema = """\
        sip.trans_depth: int .
        sip.method: string .
        sip.uri: string .
        sip.request_from: string .
        sip.request_to: string .
        sip.response_from: string .
        sip.response_to: string .
        sip.call_id: string .
        sip.seq: string .
        sip.request_path: [string] .
        sip.response_path: [string] .
        sip.status_code: int .
        sip.status_msg: string .
        sip.request_body_len: int .
        sip.response_body_len: int .
        host.user_agent: [uid] @reverse .
        user_agent.name: string @index(trigram, exact) .
        user_agent.type: string @index(exact) .
        sip.user_agent: [uid] @reverse .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Sip", ["sip.trans_depth",
                     "sip.method",
                     "sip.uri",
                     "sip.request_from",
                     "sip.request_to",
                     "sip.response_from",
                     "sip.response_to",
                     "sip.call_id",
                     "sip.seq",
                     "sip.request_path",
                     "sip.response_path",
                     "sip.status_code",
                     "sip.status_msg",
                     "sip.request_body_len",
                     "sip.response_body_len",
                     "<~connection.produced>",
                     "sip.user_agent"]),
            ("Host", ["host.user_agent"]),
            ("User_Agent", ["user_agent.name",
                            "user_agent.type",
                            "<~host.user_agent>",
                            "<~sip.user_agent>"])]


def get_mutations(sip_json, source_name):
    """
    Parsing of the sip log file with the definiton of user_agent as a distinct node.

    :param sip_json: Loaded JSON with a sip log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given sip node
    """
    id_sip = "sip" + sip_json.get("uid", "").replace(':', '_0')
    nquads_mutation = textwrap.dedent("""\
        _:{id_sip} <source> "{data_source}" .
        _:{id_sip} <dgraph.type> "Sip" .
        _:{id_sip} <sip.trans_depth> "{trans_depth}" .
        _:{id_sip} <sip.method> "{method}" .
        _:{id_sip} <sip.uri> "{uri}" .
        _:{id_sip} <sip.request_from> "{request_from}" .
        _:{id_sip} <sip.request_to> "{request_to}" .
        _:{id_sip} <sip.response_from> "{response_from}" .
        _:{id_sip} <sip.response_to> "{response_to}" .
        _:{id_sip} <sip.call_id> "{call_id}" .
        _:{id_sip} <sip.seq> "{seq}" .
        _:{id_sip} <sip.request_path> "{request_path}" .
        _:{id_sip} <sip.response_path> "{response_path}" .
        _:{id_sip} <sip.status_code> "{status_code}" .
        _:{id_sip} <sip.status_msg> "{status_msg}" .
        _:{id_sip} <sip.request_body_len> "{request_body_len}" .
        _:{id_sip} <sip.response_body_len> "{response_body_len}" .
        _:{connection_uid} <connection.produced> _:{id_sip} .
    """.format(
        data_source=source_name,
        id_sip=id_sip,
        trans_depth=sip_json.get("trans_depth", 0),
        method=sip_json.get("method", ""),
        uri=sip_json.get("uri", ""),
        request_from=sip_json.get("request_from", "").replace('"', ''),
        request_to=sip_json.get("request_to", "").replace('"', ''),
        response_from=sip_json.get("response_from", "").replace('"', ''),
        response_to=sip_json.get("response_to", "").replace('"', ''),
        call_id=sip_json.get("call_id", ""),
        seq=sip_json.get("seq", ""),
        request_path=sip_json.get("request_path", ""),
        response_path=sip_json.get("response_path", ""),
        status_code=sip_json.get("status_code", 0),
        status_msg=sip_json.get("status_msg", ""),
        request_body_len=sip_json.get("request_body_len", 0),
        response_body_len=sip_json.get("response_body_len", 0),
        connection_uid=sip_json.get("uid", "").replace(':', '_0')
    ))

    # Add user_agent to the host
    if "user_agent" in sip_json:
        nquads_mutation += textwrap.dedent("""\
            _:{host} <host.user_agent> _:{ua_id} .
            _:{ua_id} <source> "{data_source}" .
            _:{ua_id} <dgraph.type> "User_Agent" .
            _:{ua_id} <user_agent.name> "{user_agent}" .
            _:{ua_id} <user_agent.type> "sip" .
            _:{id_sip} <sip.user_agent> _:{ua_id} .
        """.format(
            host=sip_json.get("id.orig_h", "").replace(':', '_0'),
            ua_id="ua-" + hashlib.sha1(sip_json.get("user_agent").replace('"', '').replace(':', '_0').encode('utf-8')).hexdigest(),
            data_source=source_name,
            user_agent=sip_json.get("user_agent", "").replace('"', ''),
            id_sip=id_sip
        ))

    return textwrap.dedent(nquads_mutation)
