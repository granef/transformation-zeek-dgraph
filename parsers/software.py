#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse software.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings
import hashlib  # Hash operations


def get_schema():
    """
    Definition of Dgraph schema for a software log.

    :return: Defined schema as a string.
    """
    schema = """\
        software.ts: dateTime @index(hour) .
        software.software_type: string @index(exact) .
        software.name: string @index(trigram, exact) .
        software.version.major: int .
        software.version.minor: int .
        software.version.minor2: int .
        software.version.minor3: int .
        software.version.addl: string .
        software.unparsed_version: string @index(trigram, exact) .
        host.software: [uid] @reverse .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Software", ["software.ts",
                          "software.software_type",
                          "software.name",
                          "software.version.major",
                          "software.version.minor",
                          "software.version.minor2",
                          "software.version.minor3",
                          "software.version.addl",
                          "software.unparsed_version",
                          "<~host.software>"]),
            ("Host", ["host.software"])]


def get_mutations(software_json, source_name):
    """
    Parsing of the software log file and definition of file, user_agent and hostname as distinct nodes.

    :param software_json: Loaded JSON with a software log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given software node
    """
    # Node id based on timestamp, host IP, and name
    id_string = software_json.get("ts", "1970-01-01T00:00:00.000000Z") + software_json.get("host", "").replace(':', '_0') + software_json.get("name", "")
    id_software = "software" + hashlib.sha1(id_string.encode('utf-8')).hexdigest()
    nquads_mutation = textwrap.dedent("""\
        _:{id_software} <source> "{data_source}" .
        _:{id_software} <dgraph.type> "Software" .
        _:{id_software} <software.ts> "{ts}" .
        _:{id_software} <software.software_type> "{software_type}" .
        _:{id_software} <software.name> "{name}" .
        _:{id_software} <software.version.major> "{version_major}" .
        _:{id_software} <software.version.minor> "{version_minor}" .
        _:{id_software} <software.version.minor2> "{version_minor2}" .
        _:{id_software} <software.version.minor3> "{version_minor3}" .
        _:{id_software} <software.version.addl> "{version_addl}" .
        _:{id_software} <software.unparsed_version> "{unparsed_version}" .
        _:{host} <host.software> _:{id_software} .
    """.format(
        data_source=source_name,
        id_software=id_software,
        ts=software_json.get("ts", "1970-01-01T00:00:00.000000Z"),
        software_type=software_json.get("software_type", ""),
        name=software_json.get("name", ""),
        version_major=software_json.get("version.major", 0),
        version_minor=software_json.get("version.minor", 0),
        version_minor2=software_json.get("version.minor2", 0),
        version_minor3=software_json.get("version.minor3", 0),
        version_addl=software_json.get("version.addl", ""),
        unparsed_version=software_json.get("unparsed_version", ""),
        host=software_json.get("host", "").replace(':', '_0'),
    ))

    return textwrap.dedent(nquads_mutation)
