#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse notice.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings


def get_schema():
    """
    Definition of Dgraph schema for a notice log.

    :return: Defined schema as a string.
    """
    schema = """\
        notice.proto: string .
        notice.note: string .
        notice.msg: string .
        notice.sub: string .
        notice.p: int .
        notice.peer_desc: string .
        notice.actions: [string] .
        notice.suppress_for: float .
        notice.dropped: bool .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Notice", ["notice.proto",
                        "notice.note",
                        "notice.msg",
                        "notice.sub",
                        "notice.p",
                        "notice.peer_desc",
                        "notice.actions",
                        "notice.suppress_for",
                        "notice.dropped",
                        "<~connection.produced>"])]


def get_mutations(notice_json, source_name):
    """
    Parsing of the notice log file.

    :param notice_json: Loaded JSON with a notice log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given notice node
    """
    if not notice_json.get("uid", ""):
        return ""

    id_notice = "not" + notice_json.get("uid", "").replace(':', '_0')

    nquads_mutation = textwrap.dedent("""\
        _:{id_notice} <source> "{data_source}" .
        _:{id_notice} <dgraph.type> "Notice" .
        _:{id_notice} <notice.proto> "{proto}" .
        _:{id_notice} <notice.note> "{note}" .
        _:{id_notice} <notice.msg> "{msg}" .
        _:{id_notice} <notice.sub> "{sub}" .
        _:{id_notice} <notice.p> "{p}" .
        _:{id_notice} <notice.peer_desc> "{peer_desc}" .
        _:{id_notice} <notice.actions> "{actions}" .
        _:{id_notice} <notice.suppress_for> "{suppress_for}" .
        _:{id_notice} <notice.dropped> "{dropped}" .
        _:{connection_uid} <connection.produced> _:{id_notice} .
    """.format(
        data_source=source_name,
        id_notice=id_notice,
        proto=notice_json.get("proto", ""),
        note=notice_json.get("note", ""),
        msg=notice_json.get("msg", ""),
        sub=notice_json.get("sub", "").replace('\\', '\\\\'),
        p=notice_json.get("p", 0),
        peer_desc=notice_json.get("peer_desc", ""),
        actions=notice_json.get("actions", ""),
        suppress_for=notice_json.get("suppress_for", 0.0),
        dropped=str(notice_json.get("dropped", False)).lower(),
        connection_uid=notice_json.get("uid", "").replace(':', '_0')
    ))

    return textwrap.dedent(nquads_mutation)
