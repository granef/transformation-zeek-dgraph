#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Module to parse files.log file and transform it to the Dgraph database RDF format.
"""

import textwrap  # dedent() function to trim multiline strings
import hashlib   # hash operations


def get_schema():
    """
    Definition of Dgraph schema for a files log.

    :return: Defined schema as a string.
    """
    schema = """\
        files.source: string .
        files.depth: int .
        files.analyzers: [string] .
        files.mime_type: string @index(exact).
        files.duration: float .
        files.local_orig: bool .
        files.is_orig: bool .
        files.seen_bytes: int .
        files.total_bytes: int .
        files.missing_bytes: int .
        files.overflow_bytes: int .
        files.timedout: bool .
        files.fuid: [uid] @reverse .
        file.mime_type: string @index(exact).
        file.total_bytes: int .
        file.md5: string @index(exact) .
        file.sha1: string @index(exact) .
        file.sha256: string @index(exact) .
        host.provided: [uid] @reverse .
        host.obtained: [uid] @reverse .
    """
    return textwrap.dedent(schema)


def get_types():
    return [("Files", ["files.source",
                       "files.depth",
                       "files.analyzers",
                       "files.mime_type",
                       "files.duration",
                       "files.local_orig",
                       "files.is_orig",
                       "files.seen_bytes",
                       "files.total_bytes",
                       "files.missing_bytes",
                       "files.overflow_bytes",
                       "files.timedout",
                       "files.fuid",
                       "<~connection.produced>"]),
            ("Host", ["host.provided",
                      "host.obtained"]),
            ("File", ["file.mime_type",
                      "file.total_bytes",
                      "file.md5",
                      "file.sha1",
                      "file.sha256",
                      "<~files.fuid>",
                      "<~host.provided>",
                      "<~host.obtained>"])]


def get_mutations(files_json, source_name):
    """
    Parsing of the files log file with the definition of file as a distinct node.

    :param files_json: Loaded JSON with a files log line from the Zeek log
    :param source_name: Name of the data source
    :return: Full nquads definition for given files node
    """
    id_files = files_json.get("fuid", "").replace(':', '_0')
    id_file = hashlib.sha1(files_json.get("sha1", "").encode('utf-8')).hexdigest()
    files_source = files_json.get("source", "")
    connection_uid = files_json.get("uid", "")

    nquads_mutation = textwrap.dedent("""\
        _:{id_files} <source> "{data_source}" .
        _:{id_files} <dgraph.type> "Files" .
        _:{id_files} <files.source> "{source}" .
        _:{id_files} <files.depth> "{depth}" .
        _:{id_files} <files.analyzers> "{analyzers}" .
        _:{id_files} <files.mime_type> "{mime_type}" .
        _:{id_files} <files.duration> "{duration}" .
        _:{id_files} <files.local_orig> "{local_orig}" .
        _:{id_files} <files.is_orig> "{is_orig}" .
        _:{id_files} <files.seen_bytes> "{seen_bytes}" .
        _:{id_files} <files.total_bytes> "{total_bytes}" .
        _:{id_files} <files.missing_bytes> "{missing_bytes}" .
        _:{id_files} <files.overflow_bytes> "{overflow_bytes}" .
        _:{id_files} <files.timedout> "{timedout}" .
        _:{id_files} <files.fuid> _:{id_file} .
        _:{connection_uid} <connection.produced> _:{id_files} .
    """.format(
        data_source=source_name,
        id_files=id_files,
        id_file=id_file,
        source=files_source,
        depth=files_json.get("depth", 0),
        analyzers=files_json.get("analyzers", ""),
        mime_type=files_json.get("mime_type", ""),
        duration=files_json.get("duration", 0.0),
        local_orig=str(files_json.get("local_orig", False)).lower(),
        is_orig=str(files_json.get("is_orig", False)).lower(),
        seen_bytes=files_json.get("seen_bytes", 0),
        total_bytes=files_json.get("total_bytes", 0),
        missing_bytes=files_json.get("seen_bytes", 0),
        overflow_bytes=files_json.get("overflow_bytes", 0),
        timedout=str(files_json.get("timedout", False)).lower(),
        md5=files_json.get("md5", ""),
        sha1=files_json.get("sha1", ""),
        connection_uid=connection_uid,
    ))

    if files_source == "HTTP":
        nquads_mutation += textwrap.dedent("""\
            _:http-{connection_uid} <http.resp_fuid> _:{id_files} .
        """.format(
            id_files=id_files,
            connection_uid=connection_uid.replace(':', '_0')
        ))
    elif files_source == "FTP" and id_file:
        nquads_mutation += textwrap.dedent("""\
            _:ftp-{connection_uid} <ftp.fuid> _:{id_file} .
        """.format(
            id_file=id_file,
            connection_uid=connection_uid.replace(':', '_0')
        ))

    if id_file:
        # Append the file to host
        nquads_mutation += textwrap.dedent("""\
            _:{id_file} <source> "{data_source}" .
            _:{id_file} <dgraph.type> "File" .
            _:{id_file} <file.mime_type> "{mime_type}" .
            _:{id_file} <file.total_bytes> "{total_bytes}" .
            _:{id_file} <file.md5> "{md5}" .
            _:{id_file} <file.sha1> "{sha1}" .
            _:{id_file} <file.sha256> "{sha256}" .
            _:{orig_h} <host.obtained> _:{id_file}  .
            _:{resp_h} <host.provided> _:{id_file}  .
        """.format(
            data_source=source_name,
            id_file=id_file,
            mime_type=files_json.get("mime_type", ""),
            total_bytes=files_json.get("total_bytes", 0),
            md5=files_json.get("md5", ""),
            sha1=files_json.get("sha1", ""),
            sha256=files_json.get("sha256", ""),
            orig_h=files_json.get("id.orig_h", "").replace(':', '_0'),
            resp_h=files_json.get("id.resp_h", "").replace(':', '_0')
        ))

    return textwrap.dedent(nquads_mutation)
